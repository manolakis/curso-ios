//
//  RootViewController.h
//  Navigation
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController
- (IBAction)goToNextView:(id)sender;

@end
