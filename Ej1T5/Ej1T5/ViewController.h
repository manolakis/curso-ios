//
//  ViewController.h
//  Ej1T5
//
//  Created by Manuel Martín Prieto on 01/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface ViewController : UIViewController <MKMapViewDelegate>

# pragma mark - properties

@property (weak, nonatomic) IBOutlet UILabel *latitude;
@property (weak, nonatomic) IBOutlet UILabel *longitude;
@property (weak, nonatomic) IBOutlet MKMapView *map;

# pragma mark - handlers

- (IBAction)handleRefresh:(id)sender;
- (IBAction)handleAdd:(id)sender;
- (IBAction)handleDelete:(id)sender;

@end
