//
//  MyAnnotataion.h
//  Ej1T5
//
//  Created by Manuel Martín Prieto on 01/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c
                  title:(NSString *) t
               subtitle:(NSString *) st;
-(NSString *)subtitle;
-(NSString *)title;

@end
