//
//  MyAnnotataion.m
//  Ej1T5
//
//  Created by Manuel Martín Prieto on 01/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

@synthesize coordinate;

-(NSString *)subtitle {
    return subtitle;
}

-(NSString *)title {
    return title;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c
                  title:(NSString *) t
               subtitle:(NSString *) st {
    coordinate = c;
    title = [t copy];
    subtitle = [st copy];
    return self;
}

@end
