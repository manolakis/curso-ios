//
//  ViewController.m
//  Ej1T5
//
//  Created by Manuel Martín Prieto on 01/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "ViewController.h"
#import "MyAnnotation.h"

@implementation ViewController
@synthesize latitude;
@synthesize longitude;
@synthesize map;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setLatitude:nil];
    [self setLongitude:nil];
    [self setMap:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

# pragma mark - handlers

- (IBAction)handleRefresh:(id)sender {
    CLLocationCoordinate2D center = [self.map centerCoordinate];
    self.latitude.text = [NSString stringWithFormat:@"%f", center.latitude];
    self.longitude.text = [NSString stringWithFormat:@"%f", center.longitude];
}

- (IBAction)handleAdd:(id)sender {

    MyAnnotation *annotation = [[MyAnnotation alloc] initWithCoordinate:[self.map centerCoordinate] title:@"Título guay" subtitle:@"Texto súper guay"];
    [self.map addAnnotation:annotation];

}

- (IBAction)handleDelete:(id)sender {
    [self.map removeAnnotations:[self.map annotations]];
}

#pragma mark - delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    // Si es la posición del usuario devolvemos nil
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Si la anotación corresponde a nuestra clase, creamos su vista asociada aquí
    if ([annotation isKindOfClass:[MyAnnotation class]]) {
        // Intentamos reutilizar una vista ya creada
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        if (!pinView) {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            [pinView setPinColor:MKPinAnnotationColorPurple];
            pinView.canShowCallout = TRUE;
            pinView.animatesDrop = TRUE;
        } else {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    
    return nil;
}


@end
