//
//  Person.m
//  HelloWorld
//
//  Created by Manuel Martín Prieto on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize name = _name;
@synthesize surname = _surname;

-(NSString*) privateName {
    return self.name;
}

@end
