//
//  Person.h
//  HelloWorld
//
//  Created by Manuel Martín Prieto on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject 

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;

@end