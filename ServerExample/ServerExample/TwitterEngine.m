//
//  TwitterEngine.m
//  ServerExample
//
//  Created by Manuel Martín Prieto on 02/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "TwitterEngine.h"

@implementation TwitterEngine

- (NSArray *) refreshPublicTimeline {
    NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1/statuses/public_timeline.json?count=3&include_entities=true"];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLResponse *response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    NSDictionary *datos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    
    NSEnumerator *objects = [datos objectEnumerator];
    return objects.allObjects;
};

- (UIImage *) getUserImage:(NSString *) _url {
    NSURL *url = [[NSURL alloc] initWithString:_url];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLResponse *response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    UIImage *image = [[UIImage alloc] initWithData:data];
    return image;
}

@end
