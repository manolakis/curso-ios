//
//  MainViewController.m
//  ServerExample
//
//  Created by Manuel Martín Prieto on 02/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController

@synthesize twitterEngine = _twitterEngine;
@synthesize tableView = _tableView;

NSArray *tweets;

# pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.twitterEngine = [[TwitterEngine alloc] init];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Handlers

- (IBAction)handleRefresh:(id)sender {
    tweets = [self.twitterEngine refreshPublicTimeline];
    [self.tableView reloadData];
}

#pragma mark - DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tweets) {
        return [tweets count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tweets) {
        static NSString *cellIdentifier = @"tweetCell";
        
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *tweet = [tweets objectAtIndex:indexPath.row];

        cell.textLabel.text = [[tweet objectForKey:@"user"] objectForKey:@"screen_name"];
        cell.detailTextLabel.text = [tweet objectForKey:@"text"];
        UIImage *image = [self.twitterEngine getUserImage:[[tweet objectForKey:@"user"] objectForKey:@"profile_image_url"]];
        [cell.imageView setImage:image];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
