//
//  MainViewController.h
//  ServerExample
//
//  Created by Manuel Martín Prieto on 02/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitterEngine.h"

@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

#pragma mark - Attributes

@property (strong, nonatomic) TwitterEngine *twitterEngine;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Handlers

- (IBAction)handleRefresh:(id)sender;

@end
