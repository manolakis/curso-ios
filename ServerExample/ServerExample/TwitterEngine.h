//
//  TwitterEngine.h
//  ServerExample
//
//  Created by Manuel Martín Prieto on 02/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterEngine : NSObject 

/* Devuelve la lista de tweets pública */
- (NSArray *) refreshPublicTimeline;

- (UIImage *) getUserImage:(NSString *) _url;

@end
