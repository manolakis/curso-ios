//
//  AppDelegate.m
//  Ej3T1
//
//  Created by Manuel Martín Prieto on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

-(int) ampliacion1:(NSArray *) array {
    int counter = 0;
    for (NSNumber * number in array) {
        counter += [number intValue];
    }
    return counter;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSArray *myArray = [NSArray arrayWithObjects:@"cadena1",@"cadena2",@"cadena3",nil]; 
    for(int it = 0; it < [myArray count]; it++)
        NSLog(@"%@", [myArray objectAtIndex:it]);
    
    NSArray *myArray2 = [NSArray arrayWithObjects:
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         [NSNumber numberWithInt:1],
                         nil];
    
    NSLog(@"%i", [self ampliacion1:myArray2]);
    
    NSMutableArray *myArray3 = [NSMutableArray arrayWithCapacity:5];
    for (int i=0; i< 100; i++) {
        NSNumber *number = [NSNumber numberWithInt:i];
        [myArray3 addObject:number];
    }
    
    for (NSNumber * number in myArray3) {
        NSLog(@"%i", [number intValue]);
    }
    
    for (int i=[myArray3 count]-1; i >= 0; i--) {
        if (i % 2 == 0) {
            [myArray3 removeObjectAtIndex:i]; 
        }
    }
    
    NSMutableString * myString = [[NSMutableString alloc] init];
    
    for (NSNumber * number in myArray3) {
        [myString appendString:[number description]];
        [myString appendString:@" "];
    }
    
    NSLog(@"%@", myString);

    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
