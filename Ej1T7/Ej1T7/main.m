//
//  main.m
//  Ej1T7
//
//  Created by Manuel Martín Prieto on 01/12/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
