//
//  ViewController.h
//  Ej1T2
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *user;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIDatePicker *birthday;


- (IBAction)sendAction:(id)sender;
- (IBAction)deleteAction:(id)sender;

@end
