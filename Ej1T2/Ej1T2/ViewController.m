//
//  ViewController.m
//  Ej1T2
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize user;
@synthesize password;
@synthesize email;
@synthesize birthday;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.birthday setMaximumDate:[NSDate date]];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setUser:nil];
    [self setPassword:nil];
    [self setEmail:nil];
    [self setBirthday:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)sendAction:(id)sender {
}

- (IBAction)deleteAction:(id)sender {
}
@end
