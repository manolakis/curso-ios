//
//  AppDelegate.h
//  Ej1T1
//
//  Created by Manuel Martín Prieto on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(int) suma:(int)param1 y:(int)param2;

@end
