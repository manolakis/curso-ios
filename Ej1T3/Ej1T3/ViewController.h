//
//  ViewController.h
//  Ej1T3
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction) handleTapGesture:(UIGestureRecognizer *) sender;
- (IBAction) handlePinchGesture:(UIGestureRecognizer *) sender;
- (IBAction) handleRotateGesture:(UIGestureRecognizer *) sender;
- (IBAction) handleSwipeGesture:(UIGestureRecognizer *) sender;
- (IBAction) handleLongPressGesture:(UIGestureRecognizer *) sender;

@end
