//
//  ViewController.m
//  Ej1T3
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize imageView;

CGFloat lastScaleFactor = 1;
CGFloat netRotation;
BOOL imageSwipe = TRUE;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [imageView addGestureRecognizer:tapGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [imageView addGestureRecognizer:pinchGesture];
    
    //gesto de rotacion
    UIRotationGestureRecognizer *rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotateGesture:)];
    [imageView addGestureRecognizer:rotateGesture];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [imageView addGestureRecognizer:swipeGesture];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [imageView addGestureRecognizer:longPressGesture];
    
    [super viewDidLoad];
}

- (IBAction) handleTapGesture:(UIGestureRecognizer *) sender {
    if (sender.view.contentMode == UIViewContentModeScaleAspectFit)
        sender.view.contentMode = UIViewContentModeCenter;
    else
        sender.view.contentMode = UIViewContentModeScaleAspectFit;
}

- (IBAction) handlePinchGesture:(UIGestureRecognizer *) sender {
    CGFloat factor = [(UIPinchGestureRecognizer *) sender scale];
    
    if (factor > 1) {
        // aumentamos el zoom
        sender.view.transform = CGAffineTransformMakeScale(lastScaleFactor + (factor-1), lastScaleFactor + (factor-1));
    } else {
        // disminuimos el zoom
        sender.view.transform = CGAffineTransformMakeScale(lastScaleFactor * factor, lastScaleFactor * factor);
    }
    
    if (sender.state == UIGestureRecognizerStateEnded){
        if (factor > 1) {
            lastScaleFactor += (factor-1);
        } else {
            lastScaleFactor *= factor;
        }
    }
}

- (IBAction) handleRotateGesture:(UIGestureRecognizer *) sender {
    CGFloat rotation = [(UIRotationGestureRecognizer *) sender rotation];
    CGAffineTransform transform = CGAffineTransformMakeRotation(rotation + netRotation);
    sender.view.transform = transform;
    
    if (sender.state == UIGestureRecognizerStateEnded){
        netRotation += rotation;
    }
}

- (IBAction) handleSwipeGesture:(UIGestureRecognizer *) sender {
    UIImage *image = nil;
    if (imageSwipe) {
        image = [UIImage imageNamed:@"image2.png"];
    } else {
        image = [UIImage imageNamed:@"image.png"];
    }
    
    [self.imageView setImage:image];        
    imageSwipe = !imageSwipe;

}

- (IBAction) handleLongPressGesture:(UIGestureRecognizer *) sender {
    
    if (sender.state == UIGestureRecognizerStateEnded) {
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error catastrófico!"
                                                    message:@"Ya puedes entrar en modo pánico"
                                                   delegate:nil
                                          cancelButtonTitle:@"Entrar en modo pánico" otherButtonTitles:nil, 
                          nil];
        [alert show];
    }
    
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
