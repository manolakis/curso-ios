//
//  ViewController.m
//  Ej1T4
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize helloButton;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) toggleButton
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL value = [userDefaults boolForKey:@"enabled_preference"];
    self.helloButton.enabled = value;
}

- (int) loadCount
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *counterKey = @"counter_key";
    int counter = [userDefaults integerForKey:counterKey];
    counter++;
    
    [userDefaults setInteger:counter forKey:counterKey];
    [userDefaults synchronize];
    
    return counter;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self toggleButton];
    NSLog(@"Has arrancado la app %i veces", [self loadCount]);
}

- (void)viewDidUnload
{
    [self setHelloButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)handleAction:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"alerta roja" message:@"Mensaje" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
    [alertView show];
}
@end
