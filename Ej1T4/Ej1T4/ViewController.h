//
//  ViewController.h
//  Ej1T4
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *helloButton;
- (IBAction)handleAction:(id)sender;

@end
