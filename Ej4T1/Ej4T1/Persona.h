//
//  MyClass.h
//  Ej4T1
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Persona : NSObject
{
    int edad;
    double altura;
    NSString *nombre;
    NSArray *hijos;
}

@property (nonatomic, readwrite) int edad;
@property (nonatomic, readwrite) double altura;
@property (nonatomic, strong) NSString *nombre;
@property (nonatomic, strong) NSArray *hijos;

-(id)initWithEdad:(int)_edad altura:(double)_altura nombre:(NSString *)_nombre hijos:(NSArray *)_hijos;

@end
