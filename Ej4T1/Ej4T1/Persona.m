//
//  MyClass.m
//  Ej4T1
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Persona.h"

@implementation Persona

@synthesize edad, altura, nombre, hijos;

- (id) initWithEdad:(int)_edad altura:(double)_altura nombre:(NSString *)_nombre hijos:(NSArray *)_hijos {
    if (self = [super init]) {
        self.edad = _edad;
        self.altura = _altura;
        self.nombre = _nombre;
        self.hijos = _hijos;
    }
    return self;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"Me llamo %@, mido %0.2f, tengo %d años y %i hijos", 
            nombre, 
            altura, 
            edad, 
            [hijos count]];
}

@end
