//
//  ViewController.m
//  Ej3T2
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize name;
@synthesize surname;
@synthesize address;
@synthesize text;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setName:nil];
    [self setSurname:nil];
    [self setAddress:nil];
    [self setText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Actions

- (IBAction)hideKeyboard:(id)sender {
    
}

- (IBAction)showText:(id)sender {
    NSString *texto = [NSString stringWithFormat:@"%@ %@ %@", self.name.text, self.surname.text, self.address.text];
    self.text.text = texto;
}


@end
