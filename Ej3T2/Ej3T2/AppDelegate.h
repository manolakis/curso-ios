//
//  AppDelegate.h
//  Ej3T2
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
