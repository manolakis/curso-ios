//
//  ViewController.h
//  OutletsYActions
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)botonPulsado;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
