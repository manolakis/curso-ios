//
//  main.m
//  OutletsYActions
//
//  Created by Manuel Martín Prieto on 29/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, 
                                 argv, 
                                 nil, // clase UIApplicaion a utilizar. Por defecto UIApplication 
                                 NSStringFromClass([AppDelegate class])); // AppDelegate a utilizar
    }
}
