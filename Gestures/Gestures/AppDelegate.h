//
//  AppDelegate.h
//  Gestures
//
//  Created by Manuel Martín Prieto on 30/11/11.
//  Copyright (c) 2011 The RIA Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
